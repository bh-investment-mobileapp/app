import 'package:flutter/cupertino.dart';

class TextStyleUtils {
  static TextStyle subtitleStyle = TextStyle(fontSize: 12);
  static TextStyle normalStyle = TextStyle(fontSize: 14);
  static TextStyle subHeaderStyle = TextStyle(fontSize: 16);
  static TextStyle headerStyle = TextStyle(fontSize: 18);
}
