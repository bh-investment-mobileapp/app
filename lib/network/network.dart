import 'dart:async';
import 'dart:io';
import 'package:bhinvestmentmobile/network/custom_exception.dart';
import 'package:dio/dio.dart';

class Network {
  Dio dio = Dio()
    ..options.baseUrl = "Unknown"
    ..options.connectTimeout = 5000;

  Future get(String endPoint, {String token = ""}) async {
    try {
      Response response;
      if (token.isNotEmpty) {
        response =
            await dio.get(endPoint, options: Options(headers: {'Authorization': "Bearer $token"}));
      } else {
        response = await dio.get(endPoint);
      }
      return response;
    } catch (e) {
      if (e is DioError) {
        print("Dio Error ${e.response.statusCode}");
        return handleError(e);
      } else {
        print("No Error Dio Response ");
        return throw "Something went wrong";
      }
    }
  }

  Future post(String endPoint, Map<String, dynamic> body) async {
    try {
      Response response = await dio.post(endPoint, data: body);
      return response;
    } catch (e) {

      if (e is DioError) {
        print("Error ${e.message}");
        return handleError(e);
      } else {
        return throw "Something went wrong";
      }
    }
  }

  dynamic handleError(DioError error) {
    if (error.error.runtimeType == SocketException) {
      return throw "No internet connection";
    } else if (error.error.runtimeType == TimeoutException) {
      return throw "Request timeout";
    } else {
      switch (error.response.statusCode) {
        case 400:
          throw BadRequestException(error.response.data);
        case 401:

        case 404:

        case 403:
          throw BadRequestException(error.response.data);
        case 500:

        default:
          return throw FetchDataException(
              'Error occured while Communication with Server with StatusCode : ${error.response.statusCode}');
      }
    }
  }
}

final network = Network();
