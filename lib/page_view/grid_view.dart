// TODO Implement this library.
import 'package:flutter/material.dart';
class Items {
  String title;
  String event;
  String icons;
  Items({this.title,this.event,this.icons});
}
class Gridpages extends StatelessWidget {
  Items Account = new Items(
    title: 'Account',
    icons:'assets/logo.jpg' ,
  );
  Items Payment = new Items(
    title: 'Payments',
    icons:'assets/logo.jpg' ,
  );
  Items Dealers = new Items(
    title: 'Dealers',
    icons:'assets/logo.jpg' ,
  );
  Items Product = new Items(
    title: 'Products',
    icons:'assets/logo.jpg' ,
  );
  Items Carts= new Items(
    title: 'Carts',
    icons:'assets/logo.jpg' ,
  );
  Items Contact = new Items(
    title: 'Contact us',
    icons:'assets/logo.jpg' ,
  );
  Items Transfer = new Items(
    title: 'Transfer',
    icons:'assets/logo.jpg' ,
  );
  Items History = new Items(
    title: 'History',
    icons:'assets/logo.jpg' ,
  );
  Items Setting = new Items(
    title: 'Setting',
    icons:'assets/logo.jpg' ,

  );

  @override
  Widget build(BuildContext context) {
    List <Items> mylist = [
      Account,
      Payment,
      Dealers,
      Product,
      Carts,
      Contact,
      Transfer,
      History,
      Setting
    ];
    SizedBox(
      height: 40,
    );
    return Flexible(

      child: GridView.count(
        childAspectRatio: 1.0,
        padding: EdgeInsets.all(7),
        crossAxisCount: 3,
        mainAxisSpacing:5,
        crossAxisSpacing: 3,
        children: mylist.map((data)
        {
          return Container(
            decoration: BoxDecoration(
                color: Colors.black26,borderRadius: BorderRadius.circular(8)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget> [
                IconButton(icon: Image.asset(data.icons),onPressed: ()=>{},),
                SizedBox(
                  height: 14,
                ),
                Text(data.title,style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                ),)
              ],
            ),
          );
        }).toList(),
      ),
    );
  }
}
