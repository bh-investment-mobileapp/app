import 'package:bhinvestmentmobile/utils/cart_list.dart';
import 'package:bhinvestmentmobile/utils/icons_utils.dart';
import 'package:bhinvestmentmobile/utils/image_assets.dart';
import 'package:bhinvestmentmobile/utils/textstyle_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bhinvestmentmobile/page_view/grid_view.dart';



class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            images_list.image_header,
            Text("bhinvestor", style: TextStyleUtils.subHeaderStyle),
          ],
        ),
        actions: <Widget> [
          IconButton(icon: icon_list.icon_notification,),
          IconButton(icon:icon_list.icon_person),
        ],
      ),
      body: Container(
        color: Colors.greenAccent,
        child: Row(
          children: <Widget> [
            SizedBox(height: 15,),
            Gridpages(),
          ],
        ),
      ),
    );
  }
}