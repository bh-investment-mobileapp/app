import 'dart:async';

import 'package:dio/dio.dart';

class BaseStreamProvider<T> {
  StreamController<T> _controller = StreamController<T>.broadcast();

  Stream<T> get stream => _controller.stream;

  Future fetchSingleData(VoidCallback voidCallback) async {
    var result = await voidCallback.call();
    _controller.sink.add(result);
  }

  void dispose() {
    _controller.close();
  }
}
